#!/bin/bash
# Author           : Marcel Korpal
# Created On       : 2019-05-20
# Last Modified By : Marcel Korpal
# Last Modified On : 2019-05-30
# Version          : 0.1
#
# Description      :
# Graficzny interfejs do obługi repozytoriów git.
#
# Flagi: -h, -v
#
# Licensed under GPL (see /usr/share/common-licenses/GPL for more details
# or contact # the Free Software Foundation for a copy)

AUTHOR="Marcel Korpal"
VERSION="0.1"

function printHeader() {
	echo "Git-manager"
	echo ""
}

function printHelp() {
	printHeader
	echo "Opis: Graficzny manager repozytoriów git"
	echo ""
	echo "Przejdź do wybranego folderu i zarządzaj repozytorium:"
	echo "- commituj zmiany"
	echo "- pushuj commity"
	echo "- zmieniaj branche"
	echo "- dodawaj originy"
	echo ""
	echo "Opcje:"
	echo "-h wyświetla pomoc"
	echo "-v wyświetla wersję i autora"
}

function printVersion() {
	printHeader
	echo "Autor: $AUTHOR"
	echo "Wersja: $VERSION"
}

function isGitInstalled() {
	test -f /usr/bin/git
	echo $?
}

function isGitInitialized() {
	test -d .git/
	echo $?
}

function getGitEmail() {
	echo $(git config --global user.email)
}

function getGitName() {
	echo $(git config --global user.name)
}

function setGitEmail() {
	if [[ "$1" != "" ]]
	then
		echo $(git config --global user.email "$1")
	else
		zenity --info --text "Email nie został zmieniony."
	fi
}

function setGitName() {
	if [[ "$1" != "" ]]
	then
		echo $(git config --global user.name "$1")
	else
		zenity --info --text "Nazwa nie została zmieniona."
	fi
}

function getFolder() {
	pwd
}

# przechodzi do podanego folderu
function setFolder() {
	if [[ "$1" != "" ]]
	then
		cd "$1"
		if [[ $? -ne 0 ]]
		then
			zenity --error --text "Wybrany folder nie istnieje, lub nie masz do niego uprawnień."
		fi
	fi
}

# inicjalizuje repozytorium w obecnym katalogu
function initRepo() {
	git init
	if [[ $? -ne 0 ]]
	then
		zenity --error --text "Git nie mógł zainicjalizować repozytorium."
	fi
}

# pobiera repozytorium i przechodzi do folderu
function cloneRepo() {
	if [[ "$1" != "" ]]
	then
		setFolder "$2"
		git clone "$1"
		if [[ $? -ne 0 ]]
		then
			zenity --error --text "Błąd pobierania. Sprawdź uprawnienia oraz URL."
		else
			cd "$(ls -t | head -1)"
		fi
	fi
}

# zwraca listę plików nowych oraz edytowanych
function getEditedFiles() {
	git status --porcelain | cut -c 4-
}

# wyświetla listę nowych oraz edytowanych plików
function showEditedList() {
	list=($(getEditedFiles))
	zenity --list --column=Pliki "${list[@]}" --height 500 --width 400
}

# wyświetla listę originów
function showOriginList() {
	list=($(getOrigins))
	zenity --list --column=Originy "${list[@]}" --height 500 --width 400
}

function getOrigins() {
	git remote
}

function getBranches() {
	git branch | cut -c 3-
}

function getBranchName() {
	git branch | grep "\* " | cut -c 3-
}

# wyświetla listę branchy
function showBranchList() {
	list=($(getBranches))
	zenity --list --column=Branche "${list[@]}" --height 500 --width 400
}

# obsługa flag
if [[ "$1" == "-h" ]]
then
	printHelp
	exit 0
elif [[ "$1" == "-v" ]]
then
	printVersion
	exit 0
fi

if [[ "$(isGitInstalled)" != "0" ]]
then
	zenity --error --text "Program git nie jest zainstalowany. Zainstaluj go ręcznie."
	exit 1
else
	selected="0. "
	while [[ "$selected" != "" ]]
	do
		# pomiń pierwsze uruchomienie pętli
		if [[ "$selected" != "0. " ]]
		then
			selected=`zenity --list --column=Polecenie "${menu[@]}" --height 500 --width 400`
		fi

		# zachowaj identyfikator polecenia
		selected=`echo $selected | cut -d " " -f 1`

		case $selected in
			"0.");;
			"1.")
				menu=("1.1 Nazwa ($(getGitName))" "1.2 Email ($(getGitEmail))" "1.3 Wróć")
				continue;;
			"1.1") setGitName "`zenity --entry --title "Zmienne globalne" --text "Podaj nową nazwę" --entry-text="$(getGitName)"`";;
			"1.2") setGitEmail "`zenity --entry --title "Zmienne globalne" --text "Podaj nowy email" --entry-text="$(getGitEmail)"`";;
			"1.3");;
			"2.") setFolder "`zenity --file-selection --title "Folder" --text "Podaj ścieżkę do nowego folderu" --directory`";;
			"3.")
				url=`zenity --entry --title "Klonowanie" --text "Podaj adres URL"`
				if [[ "$url" != "" ]]
				then
					cloneRepo "$url" "`zenity --file-selection --title "Klonowanie" --text "Podaj folder docelowy (domyślnie w obecnym folderze)" --directory`"
				else
					zenity --info --text "Repozytorium nie zostało pobrane."
				fi;;
			"4.") initRepo;;
			"5.") showEditedList;;
			"6.")
				list=($(getEditedFiles))
				newList=()
				for el in ${list[@]}
				do
					newList+=(FALSE "$el")
				done
				toAdd=($(zenity --list --column=Select "${newList[@]}" --multiple --checklist --height 500 --width 400 --column=Pliki --separator="\n"))
				if [[ ${#toAdd[@]} -ne 0 ]]
				then
					commitName=`zenity --entry --title "Commit" --text "Podaj opis commita"`
					if [[ "$commitName" != "" ]]
					then
						for file in ${toAdd[@]}
						do
							git add "$file"
						done
						git commit -m "$commitName"
					else
						zenity --info --text "Opis commita nie może być pusty."
					fi
				else
					zenity --info --text "Brak zmian do commita."
				fi;;
			"7.")
				origin="`showOriginList`"
				if [[ "$origin" != "" ]]
				then
					zenity --info --text "Możesz zostać poproszony o autoryzację. Sprawdź konsolę."
					git push "$origin" HEAD
				else
					zenity --info --text "Zmiany nie zostały pusznięte."
				fi;;
			"8.")
				menu=("8.1 Lista" "8.2 Nowy origin" "8.3 Usuń origina" "8.4 Wróć")
				continue;;
			"8.1") showOriginList;;
			"8.2")
				originName=`zenity --entry --title "Origin" --text "Podaj nazwę origina"`
				if [[ "$originName" != "" ]]
				then
					originUrl=`zenity --entry --title "Origin" --text "Podaj URL origina"`
					if [[ "$originUrl" != "" ]]
					then
						git remote add "$originName" "$originUrl"
						if [[ $? -ne 0 ]]
						then
							zenity --info --text "Nie możesz utworzyć takiego origina."
						fi
					else
						zenity --info --text "Url nie może być pusty."
					fi
				else
					zenity --info --text "Nazwa nie może być pusta."
				fi;;
			"8.3")
				origin=`showOriginList`
				if [[ "$origin" != "" ]]
				then
					git remote rm "$origin"
				else
					zenity --info --text "Nie usunięto origina."
				fi;;
			"8.4");;
			"9.")
				menu=("9.1 Branch z listy" "9.2 Nowy branch" "9.3 Wróć")
				continue;;
			"9.1")
				branch="`showBranchList`"
				if [[ "$branch" != "" ]]
				then
					list=($(getEditedFiles))
					if [[ ${#list[@]} -ne 0 ]]
					then
						zenity --info --text "Masz niezapisane zmiany. Zapisz zmiany, aby zmienić branch."
					else
						git checkout "$branch"
					fi
				else
					zenity --info --text "Branch nie został zmieniony."
				fi;;
			"9.2")
				branch=`zenity --entry --title "Branch" --text "Podaj nazwę brancha"`
				if [[ "$branch" != "" ]]
				then
					git checkout -b "$branch"
					if [[ $? -ne 0 ]]
					then
						zenity --info --text "Nie możesz utworzyć brancha o takiej nazwie."
					fi
				else
					zenity --info --text "Branch nie został utworzony."
				fi;;
			"9.3");;
			"10.") zenity --info --title "O programie" --text "$(printVersion)";;
			"11.") zenity --info --title "Pomoc" --text "$(printHelp)";;

		esac

		menu=("1. Ustaw zmienne globalne")


		if [[ "$(getGitName)" != "" ]] && [[ "$(getGitEmail)" != "" ]]
		then
			menu+=("2. Przejdź do innego folderu ($(getFolder))")
			if [[ "$(isGitInitialized)" != "0" ]]
			then
				echo "git nie jest zainicjalizowany"
				menu+=("3. Klonuj repozytorium")
				menu+=("4. Inicjalizuj repozytorium")
			else
					menu+=("5. Lista zmian")
					menu+=("6. Commit")
					menu+=("7. Push")
					menu+=("8. Serwery zdalne")
					menu+=("9. Zmień branch ($(getBranchName))")
			fi
		fi
		menu+=("10. O programie")
		menu+=("11. Pomoc")

	done
fi
